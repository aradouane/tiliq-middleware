package com.rme;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.rme.fti.messages.reservations.ReservationRequest;
import com.rme.middleware.datasource.DAOFactory;
import com.rme.middleware.datasource.entity.Facility;
import com.rme.middleware.datasource.entity.Middle;
import com.rme.middleware.datasource.entity.Reservation;

@SpringBootTest
class Mark3ApplicationTests {
	
	@Autowired
	@Qualifier("middlejt")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ApplicationContext appContext;
	
	private static final Logger LOG = LoggerFactory.getLogger(Mark3ApplicationTests.class);

	@Test
	void contextLoads() {
		
		String temp = "<Response success=\"false\">\n" + 
				"	<Result></Result>\n" + 
				"	<Messages>\n" + 
				"		<Message>transactionId &apos;null&apos; does not refer to a valid transaction</Message>\n" + 
				"		<Message>Aborted because errors occurred during LoanIQ validation</Message>\n" + 
				"	</Messages>\n" + 
				"</Response>";
		
		Document doc = DAOFactory.convertStringToXMLDocument(temp);
		
		Node response = doc.getElementsByTagName("Messages").item(0);
		
		System.out.println(doc.getBaseURI());
		
	}

}
