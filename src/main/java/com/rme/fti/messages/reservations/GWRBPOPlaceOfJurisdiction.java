//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:02:59 AM WET 
//


package com.rme.fti.messages.reservations;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRBPOPlaceOfJurisdiction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRBPOPlaceOfJurisdiction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ctry" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRCountry" minOccurs="0"/>
 *         &lt;element name="CtrySubDvsn" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBPOCountrySubDivision1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRBPOPlaceOfJurisdiction", propOrder = {
    "ctry",
    "ctrySubDvsn"
})
public class GWRBPOPlaceOfJurisdiction {

    @XmlElementRef(name = "Ctry", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctry;
    @XmlElement(name = "CtrySubDvsn")
    protected GWRBPOCountrySubDivision1 ctrySubDvsn;

    /**
     * Gets the value of the ctry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtry() {
        return ctry;
    }

    /**
     * Sets the value of the ctry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtry(JAXBElement<String> value) {
        this.ctry = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ctrySubDvsn property.
     * 
     * @return
     *     possible object is
     *     {@link GWRBPOCountrySubDivision1 }
     *     
     */
    public GWRBPOCountrySubDivision1 getCtrySubDvsn() {
        return ctrySubDvsn;
    }

    /**
     * Sets the value of the ctrySubDvsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link GWRBPOCountrySubDivision1 }
     *     
     */
    public void setCtrySubDvsn(GWRBPOCountrySubDivision1 value) {
        this.ctrySubDvsn = value;
    }

}
