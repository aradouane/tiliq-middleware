//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:02:59 AM WET 
//


package com.rme.fti.messages.reservations;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRSettlementBaseDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRSettlementBaseDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClearingSystem" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRClearingSystemIDCode" minOccurs="0"/>
 *         &lt;element name="Intermediary" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRSIParty" minOccurs="0"/>
 *         &lt;element name="Nostro" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRNostroCode" minOccurs="0"/>
 *         &lt;element name="SundryReferenceCode" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRUserCodes" minOccurs="0"/>
 *         &lt;element name="TransferMethod" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRTransferMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRSettlementBaseDetails", propOrder = {
    "clearingSystem",
    "intermediary",
    "nostro",
    "sundryReferenceCode",
    "transferMethod"
})
@XmlSeeAlso({
    GWRPaySettlementDetails.class,
    GWRReceiveSettlementDetails.class
})
public class GWRSettlementBaseDetails {

    @XmlElement(name = "ClearingSystem")
    protected String clearingSystem;
    @XmlElement(name = "Intermediary")
    protected GWRSIParty intermediary;
    @XmlElementRef(name = "Nostro", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nostro;
    @XmlElement(name = "SundryReferenceCode")
    protected String sundryReferenceCode;
    @XmlElementRef(name = "TransferMethod", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferMethod;

    /**
     * Gets the value of the clearingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClearingSystem() {
        return clearingSystem;
    }

    /**
     * Sets the value of the clearingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClearingSystem(String value) {
        this.clearingSystem = value;
    }

    /**
     * Gets the value of the intermediary property.
     * 
     * @return
     *     possible object is
     *     {@link GWRSIParty }
     *     
     */
    public GWRSIParty getIntermediary() {
        return intermediary;
    }

    /**
     * Sets the value of the intermediary property.
     * 
     * @param value
     *     allowed object is
     *     {@link GWRSIParty }
     *     
     */
    public void setIntermediary(GWRSIParty value) {
        this.intermediary = value;
    }

    /**
     * Gets the value of the nostro property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNostro() {
        return nostro;
    }

    /**
     * Sets the value of the nostro property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNostro(JAXBElement<String> value) {
        this.nostro = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the sundryReferenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSundryReferenceCode() {
        return sundryReferenceCode;
    }

    /**
     * Sets the value of the sundryReferenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSundryReferenceCode(String value) {
        this.sundryReferenceCode = value;
    }

    /**
     * Gets the value of the transferMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferMethod() {
        return transferMethod;
    }

    /**
     * Sets the value of the transferMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferMethod(JAXBElement<String> value) {
        this.transferMethod = ((JAXBElement<String> ) value);
    }

}
