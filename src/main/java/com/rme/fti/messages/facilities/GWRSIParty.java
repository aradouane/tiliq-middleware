//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:00:40 AM WET 
//


package com.rme.fti.messages.facilities;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRSIParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRSIParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Customer" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRCustomerMnemonic" minOccurs="0"/>
 *         &lt;element name="SwiftAddress" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRSwiftAddress" minOccurs="0"/>
 *         &lt;element name="NameAndAddress" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRSSNameAndAddress" minOccurs="0"/>
 *         &lt;element name="Account" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRPartyAccountNumber" minOccurs="0"/>
 *         &lt;element name="TransliterateSwift" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBool" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRSIParty", propOrder = {
    "customer",
    "swiftAddress",
    "nameAndAddress",
    "account",
    "transliterateSwift"
})
public class GWRSIParty {

    @XmlElementRef(name = "Customer", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customer;
    @XmlElement(name = "SwiftAddress")
    protected String swiftAddress;
    @XmlElement(name = "NameAndAddress")
    protected String nameAndAddress;
    @XmlElement(name = "Account")
    protected String account;
    @XmlElement(name = "TransliterateSwift")
    protected EnigmaBoolean transliterateSwift;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomer(JAXBElement<String> value) {
        this.customer = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the swiftAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwiftAddress() {
        return swiftAddress;
    }

    /**
     * Sets the value of the swiftAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwiftAddress(String value) {
        this.swiftAddress = value;
    }

    /**
     * Gets the value of the nameAndAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameAndAddress() {
        return nameAndAddress;
    }

    /**
     * Sets the value of the nameAndAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameAndAddress(String value) {
        this.nameAndAddress = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccount(String value) {
        this.account = value;
    }

    /**
     * Gets the value of the transliterateSwift property.
     * 
     * @return
     *     possible object is
     *     {@link EnigmaBoolean }
     *     
     */
    public EnigmaBoolean getTransliterateSwift() {
        return transliterateSwift;
    }

    /**
     * Sets the value of the transliterateSwift property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnigmaBoolean }
     *     
     */
    public void setTransliterateSwift(EnigmaBoolean value) {
        this.transliterateSwift = value;
    }

}
