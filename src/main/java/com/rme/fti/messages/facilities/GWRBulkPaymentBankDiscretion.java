//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:00:40 AM WET 
//


package com.rme.fti.messages.facilities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRBulkPaymentBankDiscretion.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GWRBulkPaymentBankDiscretion">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ES"/>
 *     &lt;enumeration value="FI"/>
 *     &lt;enumeration value="HO"/>
 *     &lt;enumeration value="LI"/>
 *     &lt;enumeration value="LS"/>
 *     &lt;enumeration value="LO"/>
 *     &lt;enumeration value="DF"/>
 *     &lt;minLength value="1"/>
 *     &lt;maxLength value="2"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GWRBulkPaymentBankDiscretion")
@XmlEnum
public enum GWRBulkPaymentBankDiscretion {

    ES,
    FI,
    HO,
    LI,
    LS,
    LO,
    DF;

    public String value() {
        return name();
    }

    public static GWRBulkPaymentBankDiscretion fromValue(String v) {
        return valueOf(v);
    }

}
