//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:00:40 AM WET 
//


package com.rme.fti.messages.facilities;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWREOAPaymentResponseDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWREOAPaymentResponseDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:common.service.ti.apps.tiplus2.misys.com}GWROABaseResponseDetails">
 *       &lt;sequence>
 *         &lt;element name="ResponseParty" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRPartyRole" minOccurs="0"/>
 *         &lt;element name="ResponseAction" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWREOAPaymentResponseAction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWREOAPaymentResponseDetails", propOrder = {
    "responseParty",
    "responseAction"
})
public class GWREOAPaymentResponseDetails
    extends GWROABaseResponseDetails
{

    @XmlElementRef(name = "ResponseParty", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> responseParty;
    @XmlElement(name = "ResponseAction")
    protected String responseAction;

    /**
     * Gets the value of the responseParty property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResponseParty() {
        return responseParty;
    }

    /**
     * Sets the value of the responseParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResponseParty(JAXBElement<String> value) {
        this.responseParty = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the responseAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseAction() {
        return responseAction;
    }

    /**
     * Sets the value of the responseAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseAction(String value) {
        this.responseAction = value;
    }

}
