//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:00:40 AM WET 
//


package com.rme.fti.messages.facilities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRProductTypeParameterSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRProductTypeParameterSet">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:common.service.ti.apps.tiplus2.misys.com}GWRProductType">
 *       &lt;sequence>
 *         &lt;element name="ParameterSet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRProductTypeParameterSet", propOrder = {
    "parameterSet"
})
public class GWRProductTypeParameterSet
    extends GWRProductType
{

    @XmlElement(name = "ParameterSet")
    protected String parameterSet;

    /**
     * Gets the value of the parameterSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameterSet() {
        return parameterSet;
    }

    /**
     * Sets the value of the parameterSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameterSet(String value) {
        this.parameterSet = value;
    }

}
