//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:16:10 AM WET 
//


package com.rme.fti.messages.reversal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRMailType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GWRMailType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AM"/>
 *     &lt;enumeration value="CL"/>
 *     &lt;enumeration value="CO"/>
 *     &lt;enumeration value="FC"/>
 *     &lt;enumeration value="HD"/>
 *     &lt;enumeration value="RD"/>
 *     &lt;enumeration value="RA"/>
 *     &lt;enumeration value="RM"/>
 *     &lt;maxLength value="2"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GWRMailType")
@XmlEnum
public enum GWRMailType {

    AM,
    CL,
    CO,
    FC,
    HD,
    RD,
    RA,
    RM;

    public String value() {
        return name();
    }

    public static GWRMailType fromValue(String v) {
        return valueOf(v);
    }

}
