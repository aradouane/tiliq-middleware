//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:16:10 AM WET 
//


package com.rme.fti.messages.reversal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRExportOpenAccountParties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRExportOpenAccountParties">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:common.service.ti.apps.tiplus2.misys.com}GWROpenAccountParties">
 *       &lt;sequence>
 *         &lt;element name="BuyerBank" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRParty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRExportOpenAccountParties", propOrder = {
    "buyerBank"
})
public class GWRExportOpenAccountParties
    extends GWROpenAccountParties
{

    @XmlElement(name = "BuyerBank")
    protected GWRParty buyerBank;

    /**
     * Gets the value of the buyerBank property.
     * 
     * @return
     *     possible object is
     *     {@link GWRParty }
     *     
     */
    public GWRParty getBuyerBank() {
        return buyerBank;
    }

    /**
     * Sets the value of the buyerBank property.
     * 
     * @param value
     *     allowed object is
     *     {@link GWRParty }
     *     
     */
    public void setBuyerBank(GWRParty value) {
        this.buyerBank = value;
    }

}
