//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:16:10 AM WET 
//


package com.rme.fti.messages.reversal;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for ReservationReversal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReservationReversal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FacilityIdentifier" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRFacilityID" minOccurs="0"/>
 *         &lt;element name="FacilitySequence" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRFacilitySequence" minOccurs="0"/>
 *         &lt;element name="ReservationIdentifier" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRReservationID" minOccurs="0"/>
 *         &lt;element name="ReservationSequence" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRReservationSequence" minOccurs="0"/>
 *         &lt;element name="Product" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRProduct" minOccurs="0"/>
 *         &lt;element name="ProductSubType" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRProductTypeCode" minOccurs="0"/>
 *         &lt;element name="MasterReference" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRReference20CDT" minOccurs="0"/>
 *         &lt;element name="EventReference" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWREventReference" minOccurs="0"/>
 *         &lt;element name="Customer" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRCustomerMnemonic" minOccurs="0"/>
 *         &lt;element name="Branch" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBranchCode" minOccurs="0"/>
 *         &lt;element name="PostingKey" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRAutokeyString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservationReversal", namespace = "urn:messages.service.ti.apps.tiplus2.misys.com", propOrder = {
    "facilityIdentifier",
    "facilitySequence",
    "reservationIdentifier",
    "reservationSequence",
    "product",
    "productSubType",
    "masterReference",
    "eventReference",
    "customer",
    "branch",
    "postingKey"
})
public class ReservationReversal {

    @XmlElement(name = "FacilityIdentifier")
    protected String facilityIdentifier;
    @XmlElement(name = "FacilitySequence")
    protected String facilitySequence;
    @XmlElement(name = "ReservationIdentifier")
    protected String reservationIdentifier;
    @XmlElement(name = "ReservationSequence")
    protected String reservationSequence;
    @XmlElement(name = "Product")
    protected String product;
    @XmlElement(name = "ProductSubType")
    protected String productSubType;
    @XmlElementRef(name = "MasterReference", namespace = "urn:messages.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> masterReference;
    @XmlElement(name = "EventReference")
    protected String eventReference;
    @XmlElementRef(name = "Customer", namespace = "urn:messages.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customer;
    @XmlElementRef(name = "Branch", namespace = "urn:messages.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> branch;
    @XmlElement(name = "PostingKey")
    protected String postingKey;

    /**
     * Gets the value of the facilityIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacilityIdentifier() {
        return facilityIdentifier;
    }

    /**
     * Sets the value of the facilityIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilityIdentifier(String value) {
        this.facilityIdentifier = value;
    }

    /**
     * Gets the value of the facilitySequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacilitySequence() {
        return facilitySequence;
    }

    /**
     * Sets the value of the facilitySequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilitySequence(String value) {
        this.facilitySequence = value;
    }

    /**
     * Gets the value of the reservationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationIdentifier() {
        return reservationIdentifier;
    }

    /**
     * Sets the value of the reservationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationIdentifier(String value) {
        this.reservationIdentifier = value;
    }

    /**
     * Gets the value of the reservationSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationSequence() {
        return reservationSequence;
    }

    /**
     * Sets the value of the reservationSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationSequence(String value) {
        this.reservationSequence = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduct(String value) {
        this.product = value;
    }

    /**
     * Gets the value of the productSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductSubType() {
        return productSubType;
    }

    /**
     * Sets the value of the productSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductSubType(String value) {
        this.productSubType = value;
    }

    /**
     * Gets the value of the masterReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMasterReference() {
        return masterReference;
    }

    /**
     * Sets the value of the masterReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMasterReference(JAXBElement<String> value) {
        this.masterReference = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the eventReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventReference() {
        return eventReference;
    }

    /**
     * Sets the value of the eventReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventReference(String value) {
        this.eventReference = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomer(JAXBElement<String> value) {
        this.customer = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the branch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBranch() {
        return branch;
    }

    /**
     * Sets the value of the branch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBranch(JAXBElement<String> value) {
        this.branch = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the postingKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostingKey() {
        return postingKey;
    }

    /**
     * Sets the value of the postingKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostingKey(String value) {
        this.postingKey = value;
    }

}
