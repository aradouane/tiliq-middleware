//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:16:10 AM WET 
//


package com.rme.fti.messages.reversal;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GWRBPOPostalAddress2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GWRBPOPostalAddress2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StrtNm" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBPOStreetName" minOccurs="0"/>
 *         &lt;element name="PstCdId" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRZip" minOccurs="0"/>
 *         &lt;element name="TwnNm" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBPOTownName" minOccurs="0"/>
 *         &lt;element name="CtrySubDvsn" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRBPOCountrySubDivision" minOccurs="0"/>
 *         &lt;element name="Ctry" type="{urn:common.service.ti.apps.tiplus2.misys.com}GWRCountry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GWRBPOPostalAddress2", propOrder = {
    "strtNm",
    "pstCdId",
    "twnNm",
    "ctrySubDvsn",
    "ctry"
})
public class GWRBPOPostalAddress2 {

    @XmlElementRef(name = "StrtNm", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> strtNm;
    @XmlElementRef(name = "PstCdId", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pstCdId;
    @XmlElementRef(name = "TwnNm", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> twnNm;
    @XmlElementRef(name = "CtrySubDvsn", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctrySubDvsn;
    @XmlElementRef(name = "Ctry", namespace = "urn:common.service.ti.apps.tiplus2.misys.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctry;

    /**
     * Gets the value of the strtNm property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStrtNm() {
        return strtNm;
    }

    /**
     * Sets the value of the strtNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStrtNm(JAXBElement<String> value) {
        this.strtNm = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the pstCdId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPstCdId() {
        return pstCdId;
    }

    /**
     * Sets the value of the pstCdId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPstCdId(JAXBElement<String> value) {
        this.pstCdId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the twnNm property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTwnNm() {
        return twnNm;
    }

    /**
     * Sets the value of the twnNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTwnNm(JAXBElement<String> value) {
        this.twnNm = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ctrySubDvsn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtrySubDvsn() {
        return ctrySubDvsn;
    }

    /**
     * Sets the value of the ctrySubDvsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtrySubDvsn(JAXBElement<String> value) {
        this.ctrySubDvsn = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ctry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtry() {
        return ctry;
    }

    /**
     * Sets the value of the ctry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtry(JAXBElement<String> value) {
        this.ctry = ((JAXBElement<String> ) value);
    }

}
