//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.03 at 07:16:10 AM WET 
//


package com.rme.fti.messages.reversal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionControlEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionControlEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEW"/>
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="INHERIT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionControlEnum", namespace = "urn:control.services.tiplus2.misys.com")
@XmlEnum
public enum TransactionControlEnum {

    NEW,
    NONE,
    INHERIT;

    public String value() {
        return name();
    }

    public static TransactionControlEnum fromValue(String v) {
        return valueOf(v);
    }

}
