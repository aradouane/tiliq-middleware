package com.rme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class MiddlewareApp{

	public static void main(String[] args) {
		SpringApplication.run(MiddlewareApp.class, args);
	}

}
