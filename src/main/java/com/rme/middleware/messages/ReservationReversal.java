package com.rme.middleware.messages;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.rme.fti.messages.reversal.ObjectFactory;
import com.rme.fti.messages.reversal.ReservationReversalResponse;
import com.rme.fti.messages.reversal.ServiceResponse;

public class ReservationReversal {
	private ObjectFactory factory = new ObjectFactory();

	public String marshal(Object obj) throws JAXBException {
		Marshaller marshaller = JAXBContext.newInstance(ServiceResponse.class).createMarshaller();
		marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
		
		StringWriter sw = new StringWriter();				
		marshaller.marshal(obj, sw);
		
		return sw.toString();
	}


	public JAXBElement<ReservationReversalResponse> createJAXBElement(ReservationReversalResponse response) {
		return factory.createReservationsReversalResponse(response);
		
	}
}
