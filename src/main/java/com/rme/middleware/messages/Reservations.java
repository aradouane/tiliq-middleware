package com.rme.middleware.messages;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.rme.fti.messages.reservations.ServiceResponse;
import com.rme.fti.messages.reservations.ObjectFactory;
import com.rme.fti.messages.reservations.ReservationResponse;
import com.rme.fti.messages.reservations.ReservationResponse.ReservationResponseDetailss;
import com.rme.fti.messages.reservations.ReservationResponse.ReservationResponseDetailss.ReservationResponseDetails;
import com.rme.middleware.datasource.entity.Reservation;
import com.rme.fti.messages.reservations.ReservationResponseReservationResponseDetailsLimitCheckStatus;

public class Reservations {
	
	private ObjectFactory factory = new ObjectFactory();

	public JAXBElement<ReservationResponse> createJAXBElement(List<Reservation> reservations) {
		if(reservations == null) return null;
		
		ReservationResponse rr = new ReservationResponse();
		ReservationResponseDetailss rrds = new ReservationResponseDetailss();
			
		rr.setFacilityIdentifier(reservations.get(0).getFacilityIdentifier());
		rr.setFacilitySequence(reservations.get(0).getFacilitySequence());
		rr.setReservationIdentifier(reservations.get(0).getReservationIdentifier());
		rr.setReservationSequence(reservations.get(0).getReservationSequence());
		rr.setCustomer(factory.createReservationResponseCustomer(reservations.get(0).getCustomerName()));
		
		reservations.forEach(reservation -> {
			ReservationResponseDetails rrd = new ReservationResponseDetails();
			rrd.setDescription(reservation.getDescription());		
			rrd.setStartDate(reservation.getStartDate());
			rrd.setExpiryDate(reservation.getExpiryDate());
			rrd.setCurrency(factory.createReservationResponseReservationResponseDetailssReservationResponseDetailsCurrency(reservation.getCurrency()));
		
			BigDecimal m = new BigDecimal(reservation.getReservedAmount());
			rrd.setReservedAmount(factory.createReservationResponseReservationResponseDetailssReservationResponseDetailsReservedAmount(m.longValue()*100));
			
			rrd.setLimitCheckStatus(ReservationResponseReservationResponseDetailsLimitCheckStatus.S);
			rrd.setWarningErrorMessage(reservation.getStatus());
						
			rrds.getReservationResponseDetails().add(rrd);
		});
		
		rr.setReservationResponseDetailss(rrds);
		return factory.createReservationsResponse(rr);
	}

	public String marshal(Object obj) throws JAXBException {
		Marshaller marshaller = JAXBContext.newInstance(ServiceResponse.class).createMarshaller();
		marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
		
		StringWriter sw = new StringWriter();				
		marshaller.marshal(obj, sw);
		
		return sw.toString();
	}
}
