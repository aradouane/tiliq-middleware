package com.rme.middleware.messages;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.rme.fti.messages.facilities.EnigmaBoolean;
import com.rme.fti.messages.facilities.FacilityResponse;
import com.rme.fti.messages.facilities.FacilityResponse.FacilityDetailss;
import com.rme.fti.messages.facilities.FacilityResponse.FacilityDetailss.FacilityDetails;
import com.rme.middleware.datasource.entity.Facility;
import com.rme.fti.messages.facilities.ObjectFactory;
import com.rme.fti.messages.facilities.ServiceResponse;

public class Facilities {
	
	private ObjectFactory factory = new ObjectFactory();
	
	public JAXBElement<FacilityResponse> createJAXBElement(List<Facility> facilities) {
		FacilityResponse fr = new FacilityResponse();
		
		FacilityDetailss fds = new FacilityDetailss();
		
		facilities.forEach(facility->{
			FacilityDetails fd = new FacilityDetails();
			fd.setIdentifier(facility.getIdentifier());
			fd.setSequenceNumber(facility.getSequenceNumber());
			fd.setDescription(facility.getDescription());		
			fd.setCustomer(factory.createFacilityResponseFacilityDetailssFacilityDetailsCustomer(facility.getCustomerName()));
			fd.setRelatedPartyIdentifier(facility.getRelatedPartyIdentifier());
			fd.setStartDate(facility.getStartDate());
			fd.setExpiryDate(facility.getExpiryDate());
			fd.setCurrency(factory.createFacilityResponseFacilityDetailssFacilityDetailsCurrency(facility.getCurrency()));
			fd.setMultiCurrency(EnigmaBoolean.valueOf(facility.getMultiCurrency()));
			fd.setStatus(facility.getStatus());
			
			BigDecimal m = new BigDecimal(facility.getLimitAmount());
			fd.setLimitAmount(factory.createFacilityResponseFacilityDetailssFacilityDetailsLimitAmount(m.longValue()*100));
			
			m = new BigDecimal(facility.getLimitAmount());
			fd.setExposureAmount(factory.createFacilityResponseFacilityDetailssFacilityDetailsExposureAmount(m.longValue()*100));
			
			m = new BigDecimal(facility.getLimitAmount());
			fd.setAvailableAmount(factory.createFacilityResponseFacilityDetailssFacilityDetailsAvailableAmount(m.longValue()*100));
			
			
			fd.setDisplayField1(fd.getSequenceNumber());
			fd.setDisplayField2(fd.getIdentifier());
			fd.setDisplayField4(fd.getStatus());
			fd.setDisplayField5(fd.getCurrency().getValue());
			fd.setDisplayField8(fd.getAvailableAmount().getValue().toString());
			fd.setDisplayField9(fd.getExpiryDate());
			
			fds.getFacilityDetails().add(fd);
		});

		fr.setFacilityDetailss(fds);
		return factory.createFacilitiesResponse(fr);
	}
	
	public String marshal(Object obj) throws JAXBException {
		Marshaller marshaller = JAXBContext.newInstance(ServiceResponse.class).createMarshaller();
		marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
		
		StringWriter sw = new StringWriter();				
		marshaller.marshal(obj, sw);
		
		return sw.toString();
	}
	
}
