
package com.rme.middleware;

import java.io.StringReader;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;
import org.springframework.jms.support.JmsHeaders;

import com.rme.fti.messages.facilities.ServiceResponse;
import com.rme.fti.messages.facilities.ServiceResponse.ResponseHeader;
import com.rme.fti.messages.facilities.ServiceResponse.ResponseHeader.Details;
import com.rme.fti.messages.facilities.StatusEnum;
import com.rme.fti.messages.reservations.ReservationRequest;
import com.rme.fti.messages.reservations.ReservationResponse;
import com.rme.middleware.datasource.DAOFactory;
import com.rme.middleware.datasource.entity.Facility;
import com.rme.middleware.datasource.entity.Middle;
import com.rme.middleware.datasource.entity.Reservation;
import com.rme.middleware.messages.Facilities;
import com.rme.middleware.messages.ReservationReversal;
import com.rme.middleware.messages.Reservations;
import com.misys.tiplus2.messaging.MessagingUtils;
import com.rme.fti.messages.facilities.FacilityRequest;
import com.rme.fti.messages.facilities.FacilityResponse;
import com.rme.fti.messages.reservations.ServiceRequest;

@Component
public class MessageHandler {
	private static final Logger LOG = LoggerFactory.getLogger(MessageHandler.class);
	
	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Value("${fti.sync.queue}")
	private String syncQueueName; 
	
	
	@Value("${fti.sync.reply.queue}")
	private String syncReplyQueueName;
	
//	@Value("${fti.async.queue}")
//	private String asyncQueueName; 
	
	@Value("${fti.async.reply.queue}")
	private String asyncReplyQueueName;
	
	@JmsListener(destination = "${fti.sync.queue}")
	public void receiveSyncMessage(String msg, @Header(JmsHeaders.CORRELATION_ID) String correlationId) throws JAXBException {
		LOG.info("Synchronous message received='{}' from destination='{}'", msg, syncQueueName);
		com.rme.fti.messages.facilities.ServiceRequest sreq = (com.rme.fti.messages.facilities.ServiceRequest) (JAXBContext.newInstance(com.rme.fti.messages.facilities.ServiceRequest.class).createUnmarshaller()).unmarshal(new StringReader(msg));
		
		switch(sreq.getRequestHeader().getOperation()) {
		case "Facilities":		
			String customer = ((FacilityRequest) sreq.getRequest().get(0).getValue()).getFacilityRequestDetails().getCustomer().getValue();
			
			List<Facility> facilities = appContext.getBean(DAOFactory.class).findFacilitiesByCustomer(customer);
			
			Facilities f = new Facilities();
			JAXBElement<FacilityResponse> jaxbfr = f.createJAXBElement(facilities);
			
			ResponseHeader rh = new ResponseHeader();
			
			rh.setService(sreq.getRequestHeader().getService());
			rh.setOperation(sreq.getRequestHeader().getOperation());
			rh.setStatus(StatusEnum.SUCCEEDED);
			
			Details details = new Details();
			details.getWarning().add("LoanIQ interface testing");
			rh.setDetails(details);
			
			rh.setTargetSystem(sreq.getRequestHeader().getSourceSystem());
			rh.setSourceSystem(sreq.getRequestHeader().getTargetSystem());
			
			ServiceResponse sresp = new ServiceResponse();
			sresp.setResponseHeader(rh);
			sresp.getResponse().add(jaxbfr);
						
			send(syncReplyQueueName,  f.marshal(sresp), correlationId);
			
			break;
			
		case "Reservations":
			ServiceRequest sreq2 = (ServiceRequest) (JAXBContext.newInstance(ServiceRequest.class).createUnmarshaller()).unmarshal(new StringReader(msg));
			
			List<Middle> checkList = appContext.getBean(DAOFactory.class).getPendedReservation(
					((ReservationRequest) sreq2.getRequest().get(0).getValue()).getReservationRequestDetails().getMasterReference().getValue(),
					((ReservationRequest) sreq2.getRequest().get(0).getValue()).getReservationRequestDetails().getEventReference());
			
			com.rme.fti.messages.reservations.ServiceResponse sresp2 = new com.rme.fti.messages.reservations.ServiceResponse();
			com.rme.fti.messages.reservations.ServiceResponse.ResponseHeader rh2 = new com.rme.fti.messages.reservations.ServiceResponse.ResponseHeader();
			com.rme.fti.messages.reservations.ServiceResponse.ResponseHeader.Details details2 = new com.rme.fti.messages.reservations.ServiceResponse.ResponseHeader.Details();
			
			List<Reservation> reservations = null;
			Reservations r = new Reservations();
			
			rh2.setService(sreq.getRequestHeader().getService());
			rh2.setOperation(sreq.getRequestHeader().getOperation());
			rh2.setStatus(com.rme.fti.messages.reservations.StatusEnum.SUCCEEDED);			
			rh2.setTargetSystem(sreq.getRequestHeader().getSourceSystem());
			rh2.setSourceSystem(sreq.getRequestHeader().getTargetSystem());

			if(checkList.isEmpty()) {
				reservations = appContext.getBean(DAOFactory.class).CreateReservation(((ReservationRequest) sreq2.getRequest().get(0).getValue()).getReservationRequestDetails());
				
				if(reservations == null) {
					details2.getError().add("An error has occured while creating reservation.");			
				}else {
					JAXBElement<ReservationResponse> jaxbrr = r.createJAXBElement(reservations);
					
					if (jaxbrr != null) {
						sresp2.getResponse().add(jaxbrr);
						//INSERT RESERVATION IN MIDDLE.RESERVATIONS TABLE		
						Middle middle = new Middle(MessagingUtils.parseRequest(msg, "MasterReference"),
								MessagingUtils.parseRequest(msg, "EventReference"),
								MessagingUtils.parseRequest(msg, "FacilityIdentifier"),
								reservations.get(0).getReservationIdentifier(),
								reservations.get(0).getObjectId(),
								MessagingUtils.parseRequest(msg, "ValueDate"),
								reservations.get(0).getExpiryDate(),
								"A");
												
						NodeList res = appContext.getBean(DAOFactory.class).validateReservation(middle);
						
						if(res == null) {
							details2.getWarning().add("LoanIQ interface testing");
							appContext.getBean(DAOFactory.class).saveReservation(middle);
							
						}else {
							for(int i=0; i<res.getLength(); i++) {
								details2.getError().add(res.item(i).getTextContent());
							}
						}
					}
				}
			}
			
			rh2.setDetails(details2);
			sresp2.setResponseHeader(rh2);
			
			send(syncReplyQueueName, r.marshal(sresp2), correlationId);

			break;
			
		case "ReservationsReversal":
			if(MessagingUtils.parseRequest(msg, "ReservationIdentifier") == null) return;
			
			com.rme.fti.messages.reversal.ServiceRequest sreq3 = (com.rme.fti.messages.reversal.ServiceRequest) (JAXBContext.newInstance(com.rme.fti.messages.reversal.ServiceRequest.class).createUnmarshaller()).unmarshal(new StringReader(msg));
			com.rme.fti.messages.reversal.ServiceResponse sresp3 = new com.rme.fti.messages.reversal.ServiceResponse();
			
			com.rme.fti.messages.reversal.ServiceResponse.ResponseHeader rh3 = new com.rme.fti.messages.reversal.ServiceResponse.ResponseHeader();
			rh3.setService(sreq3.getRequestHeader().getService());
			rh3.setOperation(sreq3.getRequestHeader().getOperation());
			rh3.setStatus(com.rme.fti.messages.reversal.StatusEnum.SUCCEEDED);
			rh3.setTargetSystem(sreq3.getRequestHeader().getSourceSystem());
			rh3.setSourceSystem(sreq3.getRequestHeader().getTargetSystem());
			
			String masterReference = ((com.rme.fti.messages.reversal.ReservationReversal) sreq3.getRequest().get(0).getValue()).getMasterReference().getValue();
			String eventReference = ((com.rme.fti.messages.reversal.ReservationReversal) sreq3.getRequest().get(0).getValue()).getEventReference();
			
			List<Middle> checkList3 = appContext.getBean(DAOFactory.class).getPendedReservation(masterReference, eventReference);
			
			
			if(!checkList3.isEmpty()) {
				boolean reversed = appContext.getBean(DAOFactory.class).createReversal(checkList3.get(0).getObjectId());
				
				if(reversed) {							
					//UPDATE STATUS TO OBSOLETE IN MIDDLE.RESERVATIONS TABLE					
					appContext.getBean(DAOFactory.class).updateReservation(masterReference, eventReference, checkList3.get(0).getReservationId(), Middle.STATUS.O);	

				}else{
					rh3.setStatus(com.rme.fti.messages.reversal.StatusEnum.FAILED);
					com.rme.fti.messages.reversal.ServiceResponse.ResponseHeader.Details d = new com.rme.fti.messages.reversal.ServiceResponse.ResponseHeader.Details();
					d.getError().add("An error has occured while canceling customer reservation. Please check the correspondant drawdown outstanding on LoanIQ ");
					rh3.setDetails(d);
				}
				
				sresp3.setResponseHeader(rh3);
			}
			
			ReservationReversal reversal = new ReservationReversal();			
			send(syncReplyQueueName, reversal.marshal(sresp3), correlationId);

			break;
			
		case "NLOANR":
			
			//SEND RELEASE REQUEST
			LOG.info(msg);
			com.rme.fti.messages.reversal.ServiceRequest sreq4 = (com.rme.fti.messages.reversal.ServiceRequest) (JAXBContext.newInstance(com.rme.fti.messages.reversal.ServiceRequest.class).createUnmarshaller()).unmarshal(new StringReader(msg));
						
			masterReference = stripCDATA(MessagingUtils.parseRequest(msg, "MasterReference"));
			eventReference = MessagingUtils.parseRequest(msg, "EventReference");
			//TODO
			//CREATE Release API request
			List<Middle> middles = appContext.getBean(DAOFactory.class).getPendedReservation(masterReference, eventReference);
			StringBuilder sbDetails = new StringBuilder();
			
			for(Middle m : middles) {
				NodeList res = appContext.getBean(DAOFactory.class).releaseDrawdown(m.getObjectId(), m.getEffectiveDate(), m.getExpiryDate());
				if(res == null) {
					appContext.getBean(DAOFactory.class).updateReservation(masterReference, eventReference, m.getReservationId(), Middle.STATUS.R);
					
					String response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
							"<ServiceResponse xmlns=\"urn:control.services.tiplus2.misys.com\" xmlns:ns2=\"urn:messages.service.ti.apps.tiplus2.misys.com\" xmlns:ns3=\"urn:common.service.ti.apps.tiplus2.misys.com\">\n" + 
							"	<ResponseHeader>\n" + 
							"		<Service>GATEWAY</Service>\n" + 
							"		<Operation>NLOANR</Operation>\n" + 
							"		<Status>SUCCEEDED</Status>\n" + 
							"		<Details>\n" + 
							"			<Warning>MARK TRANSPORT CLIENT RESPONSE: Request sent successfully</Warning>\n" + 
							"		</Details>\n" + 
							"		<CorrelationId>" + sreq.getRequestHeader().getCorrelationId() + "</CorrelationId>\n" + 
							"		<TargetSystem>ZONE1</TargetSystem>\n" + 
							"		<SourceSystem>MARKXS</SourceSystem>\n" + 
							"	</ResponseHeader>\n" + 
							"</ServiceResponse>";

					send(syncReplyQueueName, response, correlationId);
				}else {
					sbDetails.append("<Details>\n");
					for(int i=0; i<res.getLength(); i++) {
						sbDetails.append("<Error>" + res.item(i).getTextContent() + "</Error>\n");
					}
					sbDetails.append("</Details>\n");
				}	
			}		
			
			//TODO
			//CREATE DEFERRED RESPONSE 
			
			String response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
					"<DeferredResponse xmlns=\"urn:control.services.tiplus2.misys.com\" xmlns:c=\"urn:common.service.ti.apps.tiplus2.misys.com\" xmlns:m=\"urn:messages.service.ti.apps.tiplus2.misys.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" + 
					"	<DeferredHeader>\n" + 
					"		<Service>TI</Service>\n" + 
					"		<Operation>NLRELS</Operation>\n" + 
					"		<Credentials>\n" + 
					"			<Name>SUPERVISOR</Name>\n" + 
					"		</Credentials>\n" + 
					"		<ReplyFormat>FULL</ReplyFormat>\n" + 
					"		<Status>SUCCEEDED</Status>\n" +
							sbDetails.toString() + 
					"		<TargetSystem>ZONE1</TargetSystem>\n" + 
					"		<SourceSystem>MARKXS</SourceSystem>\n" + 
					"		<CorrelationId>" + sreq4.getRequestHeader().getCorrelationId() + "</CorrelationId>\n" + 
					"	</DeferredHeader>\n" + 
					"	<m:TFEXRRSP>\n" + 
					"		<m:Context>\n" + 
					"			<c:Product>" + MessagingUtils.parseRequest(msg, "Product") + "</c:Product>\n" + 
					"			<c:Event>" + MessagingUtils.parseRequest(msg, "Event") + "</c:Event>\n" + 
					"			<c:OurReference>" + stripCDATA(MessagingUtils.parseRequest(msg, "MasterReference")) + "</c:OurReference>\n" + 
					"			<c:EventReference>" + MessagingUtils.parseRequest(msg, "EventReference") +"</c:EventReference>\n" + 
					"			<c:Step>NLRELS</c:Step>\n" + 
					"		</m:Context>\n" + 
					"		<m:AutoComplete>Y</m:AutoComplete>\n" + 
					"	</m:TFEXRRSP>\n" + 
					"</DeferredResponse>";
			
			send(asyncReplyQueueName, response, null);
			
			break;
			
		default:
			break;
		}
	}
	
	public void send(String destination, String message, String correlationId) {
		LOG.info("sending message='{}' to destination='{}'", message, destination);
		
		jmsTemplate.convertAndSend(destination, message, new MessagePostProcessor() {
	        public Message postProcessMessage(Message message) throws JMSException {
	        	if(correlationId != null)
	        		message.setJMSCorrelationID(correlationId);
	            message.setJMSPriority(5);
	            return message;
	        }
	    });
	}
	
	public static String stripCDATA(String s) {
		LOG.error("MARK: " + s);
	    s = s.trim();
	    if (s.startsWith("<![CDATA[")) {
	      s = s.substring(9);
	      int i = s.indexOf("]]>");
	      if (i == -1) {
	        throw new IllegalStateException(
	            "argument starts with <![CDATA[ but cannot find pairing ]]&gt;");
	      }
	      s = s.substring(0, i);
	    }
	    return s;
	  }


}