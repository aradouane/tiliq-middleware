package com.rme.middleware.datasource;

import java.io.StringReader;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;

import com.misys.liq.common.exception.LoanIQException;
import com.misys.liq.connect.APIServerConnection;
import com.misys.liq.connect.APIServerConnectionFactory;
import com.rme.fti.messages.reservations.GWRReservation;
import com.rme.middleware.MessageHandler;
import com.rme.middleware.datasource.entity.Facility;
import com.rme.middleware.datasource.entity.Middle;
import com.rme.middleware.datasource.entity.Reservation;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DAOFactory {
	private static final Logger LOG = LoggerFactory.getLogger(DAOFactory.class);
		
	@Autowired
	@Qualifier("liqjt")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("middlejt")
	JdbcTemplate middleJT;
	
	@Value("${liq.api.http.host}")
	private String httpHost; 
	
	@PostConstruct
	public void init() {
		//middleJT.execute("DROP TABLE IF EXISTS Reservations");
		middleJT.execute("CREATE TABLE IF NOT EXISTS Reservations(Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ " MasterReference VARCHAR(16),"
				+ " EventReference VARCHAR(8),"
				+ " FacilityId VARCHAR(8),"
				+ " ReservationId VARCHAR(8),"
				+ " ObjectId VARCHAR(24),"
				+ " EffectiveDate DATE,"
				+ " ExpiryDate DATE,"
				+ " Status VARCHAR(1))");
	}
		
	public List<Facility> findFacilitiesByCustomer(String customer){
		return jdbcTemplate.query("Select FAC_PID_FACILITY Identifier,\n" + 
				"FAC_NUM_FAC_CNTL SequenceNumber,\n" + 
				"FAT_DSC_FAC_TYPE Description,\n" + 
				"CUS_NME_FULL_NAME CustomerName,\n" + 
				"CUS_XID_CUST_ID RelatedPartyIdentifier,\n" + 
				"FAC_DTE_EFFECTIVE StartDate,\n" + 
				"FAC_DTE_EXPIRY ExpiryDate,\n" + 
				"FAC_CDE_Currency Currency,\n" + 
				"FAC_AMT_GLOBL_CURR LimitAmount,\n" + 
				"FAC_IND_MULTI_CURR MultiCurrency,\n" + 
				"SUM(OST_AMT_FC_CURRENT)  ExposureAmount,\n" + 
				"FAV_AMT_GL_AVAIL AvailableAmount,\n" + 
				"FSS_DSC_STATUS Status \n" + 
				"From LS2USER.VLS_FACILITY FAC \n" + 
				"Inner join LS2USER.VLS_FAC_BORR_DETL FBD on FBD_PID_FACILITY = FAC_PID_FACILITY \n" + 
				"Inner join LS2USER.VLS_DEAL_BORROWER DBR on FBD_RID_DEAL_BORR = DBR_RID_DEAL_BORR \n" + 
				"Inner join LS2USER.VLS_CUSTOMER CUS on DBR_CID_CUST_ID = CUS_CID_CUST_ID \n" + 
				"Inner join LS2USER.VLS_FACILITY_TYPE on FAC_CDE_FAC_TYPE = FAT_CDE_FAC_TYPE \n" + 
				"LEFT JOIN LS2USER.VLS_OUTSTANDING ON FAC_PID_FACILITY = OST_PID_FACILITY \n" + 
				"Inner join LS2USER.VRP_FAC_AVAIL_DRAW on FAV_PID_FACILITY = FAC_PID_FACILITY \n" + 
				"Inner join LS2USER.VRP_FAC_STATUS on FSS_PID_FACILITY = FAC_PID_FACILITY \n" + 
				"Where CUS_NME_FULL_NAME = ? \n" + 
				"GROUP BY FAC_PID_FACILITY,FAC_NUM_FAC_CNTL,FAT_DSC_FAC_TYPE,CUS_NME_FULL_NAME,FAC_DTE_EFFECTIVE,FAC_DTE_EXPIRY,FAC_CDE_Currency,FAC_AMT_GLOBL_CURR,\n" + 
				"FAC_IND_MULTI_CURR,FAV_AMT_GL_AVAIL,FSS_DSC_STATUS, CUS_XID_CUST_ID", 
	    		new Object[] { customer },
	            (rs, rowNum) -> new Facility(rs.getString("Identifier").trim(), 
	            		rs.getString("SequenceNumber").trim(), 
	            		rs.getString("Description").trim(),
	            		rs.getString("CustomerName"),
	            		rs.getString("RelatedPartyIdentifier").trim(),
	            		rs.getString("StartDate"), 
	            		rs.getString("ExpiryDate"), 
	            		rs.getString("Currency"), 
	            		rs.getString("LimitAmount"), 
	            		rs.getString("MultiCurrency"), 
	            		rs.getString("ExposureAmount"), 
	            		rs.getString("AvailableAmount"), 
	            		rs.getString("Status")));
		
	}
	
	public List<Reservation> getReservationByAlias(String alias) {
		return jdbcTemplate.query("Select FAC_PID_FACILITY	FacilityIdentifier,\n" + 
				"FAC_NUM_FAC_CNTL FacilitySequence,\n" + 
				"OST_RID_OUTSTANDNG ReservationIdentifier,\n" + 
				"OST_NME_ALIAS ReservationSequence,\n" + 
				"CUS_NME_FULL_NAME CustomerName,\n" + 
				"OST_CDE_OUTSTD_TYP Description,\n" + 
				"OTR_DTE_EFFECTIVE StartDate,\n" + 
				"OST_DTE_EXPIRY_CLC ExpiryDate,\n" + 
				"OST_CDE_CURRENCY Currency,\n" + 
				"OST_AMT_ORIGINAL ReservedAmount\n" + 
				"from LS2USER.VLS_OUTSTANDING\n" + 
				"INNER JOIN LS2USER.VLS_FACILITY on FAC_PID_FACILITY = OST_PID_FACILITY\n" + 
				"Inner join LS2USER.VLS_FAC_BORR_DETL FBD on FBD_PID_FACILITY = FAC_PID_FACILITY\n" + 
				"Inner join LS2USER.VLS_DEAL_BORROWER DBR on FBD_RID_DEAL_BORR = DBR_RID_DEAL_BORR\n" + 
				"Inner join LS2USER.VLS_CUSTOMER on DBR_CID_CUST_ID = CUS_CID_CUST_ID\n" + 
				"Inner join LS2USER.VLS_OST_TRAN on OTR_RID_OUTSTANDNG = OST_RID_OUTSTANDNG\n" + 
				"Where OST_NME_ALIAS = ?",
				new Object[] {alias},
				(rs, rowNum) -> new Reservation(
						rs.getString("FacilityIdentifier"),
						rs.getString("FacilitySequence").trim(),
						rs.getString("ReservationIdentifier"),
						rs.getString("ReservationSequence"),
						rs.getString("CustomerName"),
						rs.getString("Description"),
						rs.getString("StartDate"),
						rs.getString("ExpiryDate"),
						rs.getString("Currency"),
						rs.getString("ReservedAmount")));
	}
	
	public List<Reservation> CreateReservation(GWRReservation reservation) {
		XMLGregorianCalendar expiryDate = reservation.getValueDate().getValue();
		expiryDate.setDay(expiryDate.getDay() + 10);
		
		String xmlMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<!DOCTYPE CreateNonLoanDrawdown SYSTEM \"CreateNonLoanDrawdown.dtd\">\n" + 
				"<CreateNonLoanDrawdown\n" + 
				"	 version=\"1.0\"\n" + 
				"	 borrowerExternalID=\""+reservation.getRelatedPartyIdentifier()+"\"\n" + 
				"	 currency=\""+reservation.getPostingAmount().getCurrency().getValue()+"\"\n" + 
				"	 effectiveDate=\""+reservation.getValueDate().getValue()+"\"\n" + 
				"	 pricingOption=\"SFIDS\"\n" + 
				"	 requestedAmount=\""+reservation.getPostingAmount().getAmount().getValue()/100+"\"\n" + 
				"	 riskType=\"LAIDS\"\n" + 
				"	 expiryDate=\""+expiryDate+"\"\n" + 
				"	 facilityId=\""+reservation.getFacilityIdentifier()+"\"\n" + 
				"	 statusCode=\"AWA\">\n" + 
				"</CreateNonLoanDrawdown>";
		
		LOG.info(xmlMsg);
				
		try {
			String xmlRsp = getConnection().execute(xmlMsg);
			
			LOG.info("MARK: " + xmlRsp);
			
			Document doc = convertStringToXMLDocument(xmlRsp);
			
			if(doc == null) return null;
			
			Node response = doc.getElementsByTagName("Response").item(0);
			
			if("true".equals(response.getAttributes().getNamedItem("success").getNodeValue())) {
				Node NonLoanAsReturnValue = doc.getElementsByTagName("NonLoanAsReturnValue").item(0);
				List<Reservation> res = getReservationByAlias(NonLoanAsReturnValue.getAttributes().getNamedItem("alias").getNodeValue());
				res.get(0).setStatus(NonLoanAsReturnValue.getAttributes().getNamedItem("status").getNodeValue());
				res.get(0).setReservationIdentifier(NonLoanAsReturnValue.getAttributes().getNamedItem("outstandingId").getNodeValue());
				res.get(0).setObjectId(NonLoanAsReturnValue.getAttributes().getNamedItem("objectId").getNodeValue());
				return res;
			}else {
				LOG.error("", xmlRsp);		
			}
			
		} catch (LoanIQException e) {
			LOG.error("", e);
		}
		return null;
	}
	
	public boolean createReversal(String ReservationIdentifier) {

		String xmlMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<!DOCTYPE DeleteNonLoanDrawdown SYSTEM \"DeleteNonLoanDrawdown.dtd\">\n" + 
				"\n" + 
				"<DeleteNonLoanDrawdown\n" + 
				"	 version=\"1.0\"\n" + 
				"	 objectId=\""+ReservationIdentifier+"\"/>";
		
		try {
			String xmlRsp = getConnection().execute(xmlMsg);
			
			System.out.println(xmlRsp);
			
			Document doc = convertStringToXMLDocument(xmlRsp);
			
			Node response = doc.getElementsByTagName("Response").item(0);
			
			if("true".equals(response.getAttributes().getNamedItem("success").getNodeValue())) {
				return true;
			}
			
		} catch (LoanIQException e) {
			LOG.error("", e);
		}
		return false;
	}
	
	
	public APIServerConnection getConnection() {
		Hashtable<String,String> params = new Hashtable<String, String>();
		params.put("httpUrl", httpHost);
		try {
			return APIServerConnectionFactory.getInstance().getAPIServerConnection(params, "HTTP");
		} catch (LoanIQException e) {
			return null;
		}
	}
	
    public static Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
        	LOG.error("convertStringToXMLDocument: ", e);
        }
        return null;
    }

	public NodeList releaseDrawdown(String objectId, String effectiveDate, String expiryDate) {
		String xmlMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<!DOCTYPE UpdateNonLoanDrawdown SYSTEM \"UpdateNonLoanDrawdown.dtd\">\n" + 
				"\n" + 
				"<UpdateNonLoanDrawdown\n" + 
				"	 version=\"1.0\"\n" + 
				"	 objectId=\""+objectId+"\"\n" + 
				"	 effectiveDate=\""+effectiveDate+"\"\n" + 
				"	 expiryDate=\""+expiryDate+"\"\n" + 
				"	 statusCode=\"RELSD\"/>";
		
		LOG.info(xmlMsg);
		
		try {
			String xmlRsp = getConnection().execute(xmlMsg);
			
			LOG.info("MARK: " + xmlRsp);
			
			Document doc = convertStringToXMLDocument(xmlRsp);
						
			return doc.getElementsByTagName("Message");	
			
		} catch (LoanIQException e) {
			LOG.error("", e);
		}
		return null;
	}
	
	public int updateReservation(String masterReference, String eventReference, String reservationId, Middle.STATUS status){
		return middleJT.update("UPDATE Reservations set STATUS = ? where MasterReference = ? and EventReference = ? and ReservationId = ?", 
	    		new Object[] { status, masterReference, eventReference, reservationId});
	}
	
	public List<Middle> getPendedReservation(String MasterReference, String EventReference){
		return middleJT.query("SELECT * FROM Reservations WHERE MasterReference = ? and EventReference = ? and Status = 'A'", 
	    		new Object[] { MasterReference, EventReference},
	            (rs, rowNum) -> new Middle(rs.getString("Id").trim(), 
	            		rs.getString("MasterReference"),
	            		rs.getString("EventReference"),
	            		rs.getString("FacilityId"),
	            		rs.getString("ReservationId"),
	            		rs.getString("ObjectId"),
	            		rs.getString("EffectiveDate"),
	            		rs.getString("ExpiryDate"),
	            		rs.getString("Status")));
	}

	public int saveReservation(Middle middle) {
		
		return middleJT.update("INSERT INTO Reservations(MasterReference, EventReference, FacilityId, ReservationId, ObjectId, EffectiveDate, ExpiryDate, Status)"
				+ " VALUES('" + middle.getMasterReference() 
				+ "', '" + middle.getEventReference() 
				+ "', '" + middle.getFacilityId() 
				+ "', '" + middle.getReservationId() 
				+ "', '" + middle.getObjectId() 
				+ "', '" + middle.getEffectiveDate() 
				+ "', '" + middle.getExpiryDate() 
				+ "', '" + middle.getStatus() + "')");   
	}

	public NodeList validateReservation(Middle reservation) {
		
		String xmlMsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<!DOCTYPE UpdateNonLoanDrawdown SYSTEM \"UpdateNonLoanDrawdown.dtd\">\n" + 
				"\n" + 
				"<UpdateNonLoanDrawdown\n" + 
				"	 version=\"1.0\"\n" + 
				"	 objectId=\""+reservation.getObjectId()+"\"\n" + 
				"	 effectiveDate=\""+reservation.getEffectiveDate()+"\"\n" + 
				"	 expiryDate=\""+reservation.getExpiryDate()+"\"\n" + 
				"	 statusCode=\"PEND\"/>";
		
		LOG.info(xmlMsg);
		
		try {
			String xmlRsp = getConnection().execute(xmlMsg);
			
			LOG.info("MARK: " + xmlRsp);
			
			Document doc = convertStringToXMLDocument(xmlRsp);
			
			return doc.getElementsByTagName("Message");	
			
		} catch (LoanIQException e) {
			LOG.error("", e);
		}
		return null;
	}
	
}
