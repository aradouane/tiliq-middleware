package com.rme.middleware.datasource.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DBConfig {
	
	@Bean(name = "loaniq")
	@ConfigurationProperties(prefix = "spring.datasource.liq")
	public DataSource LiqDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "liqjt")
	public JdbcTemplate LiqJdbcTemplate(@Qualifier("loaniq") DataSource ds) {
		return new JdbcTemplate(ds);
	}

	@Bean(name = "middle")
	@ConfigurationProperties(prefix = "spring.datasource.middle")
	public DataSource MiddleDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "middlejt")
	public JdbcTemplate MiddleJdbcTemplate(@Qualifier("middle") DataSource ds) {
		return new JdbcTemplate(ds);
	}

}
