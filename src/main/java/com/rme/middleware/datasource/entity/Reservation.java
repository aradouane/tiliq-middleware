package com.rme.middleware.datasource.entity;

import com.rme.fti.messages.reservations.GWRMoney;

public class Reservation {
	
	private String FacilityIdentifier;
	private String FacilitySequence;
	private String ReservationIdentifier;
	private String ReservationSequence;
	private String CustomerName;
	private String Description;
	private String StartDate;
	private String ExpiryDate;
	private String Currency;
	private String ReservedAmount;
	private GWRMoney requestedAmt;
	private String Status;
	private String ObjectId;

	public Reservation(String FacilityIdentifier, String FacilitySequence, String ReservationIdentifier, String ReservationSequence, String CustomerName, String Description,
			String StartDate, String ExpiryDate, String Currency, String ReservedAmount) {
		
		this.FacilityIdentifier = FacilityIdentifier;
		this.FacilitySequence = FacilitySequence;
		this.ReservationIdentifier = ReservationIdentifier;
		this.ReservationSequence = ReservationSequence;
		this.CustomerName = CustomerName;
		this.Description = Description;
		this.StartDate = StartDate;
		this.ExpiryDate = ExpiryDate;
		this.Currency = Currency;
		this.ReservedAmount = ReservedAmount;
	}

	public String getFacilitySequence() {
		return FacilitySequence;
	}

	public void setFacilitySequence(String facilitySequence) {
		FacilitySequence = facilitySequence;
	}

	public String getReservationSequence() {
		return ReservationSequence;
	}

	public void setReservationSequence(String reservationSequence) {
		ReservationSequence = reservationSequence;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getExpiryDate() {
		return ExpiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getReservedAmount() {
		return ReservedAmount;
	}

	public void setReservedAmount(String reservedAmount) {
		ReservedAmount = reservedAmount;
	}

	public void setFacilityIdentifier(String facilityIdentifier) {
		FacilityIdentifier = facilityIdentifier;
	}

	public void setReservationIdentifier(String reservationIdentifier) {
		ReservationIdentifier = reservationIdentifier;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
	public void setStatus(String status) {
		Status = status;
	}

	public GWRMoney getRequestedAmt() {
		return requestedAmt;
	}

	public void setRequestedAmt(GWRMoney requestedAmt) {
		this.requestedAmt = requestedAmt;
	}
	
	public String getFacilityIdentifier() {
		return this.FacilityIdentifier;
	}

	public String getReservationIdentifier() {
		return this.ReservationIdentifier;
	}

	public String getDescription() {
		return this.Description;
	}

	public String getStatus() {
		return this.Status;
	}
	
	public String getObjectId() {
		return this.ObjectId;
	}

	public void setObjectId(String objectId) {
		ObjectId = objectId;
		
	}

}
