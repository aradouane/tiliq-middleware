package com.rme.middleware.datasource.entity;

public class Middle {
	
	public enum STATUS{
		A, R, F, O;
	}

	private String Id, MasterReference, EventReference, FacilityId, ReservationId, ObjectId, EffectiveDate, ExpiryDate, Status;
	
	public Middle(String masterReference, String eventReference, String facilityId, String reservationId, String objectId,
			String effectiveDate, String expiryDate, String status) {
		MasterReference = masterReference;
		EventReference = eventReference;
		FacilityId = facilityId;
		ReservationId = reservationId;
		ObjectId = objectId;
		EffectiveDate = effectiveDate;
		ExpiryDate = expiryDate;
		Status = status;		
	}

	public Middle(String id, String masterReference, String eventReference, String facilityId, String reservationId,  String objectId, 
			String effectiveDate, String expiryDate, String status) {
		Id = id;
		MasterReference = masterReference;
		EventReference = eventReference;
		FacilityId = facilityId;
		ReservationId = reservationId;
		ObjectId = objectId;
		EffectiveDate = effectiveDate;
		ExpiryDate = expiryDate;
		Status = status;
	}
	
	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getMasterReference() {
		return MasterReference;
	}

	public void setMasterReference(String masterReference) {
		MasterReference = masterReference;
	}

	public String getEventReference() {
		return EventReference;
	}

	public void setEventReference(String eventReference) {
		EventReference = eventReference;
	}

	public String getFacilityId() {
		return FacilityId;
	}

	public void setFacilityId(String facilityId) {
		FacilityId = facilityId;
	}

	public String getReservationId() {
		return ReservationId;
	}
	
	public String getObjectId() {
		return ObjectId;
	}

	public void setReservationId(String reservationId) {
		ReservationId = reservationId;
	}

	public void setObjectId(String objectId) {
		ObjectId = objectId;
	}

	public String getEffectiveDate() {
		return EffectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		EffectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
		return ExpiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ id=").append(this.Id)
		.append(", MasterReference=").append(this.MasterReference)
		.append(", EventReference=").append(this.EventReference)
		.append(", FacilityId=").append(this.FacilityId)
		.append(", ReservationId=").append(this.ReservationId)
		.append(", ObjectId=").append(this.ObjectId)
		.append(", EffectiveDate=").append(this.EffectiveDate)
		.append(", ExpiryDate=").append(this.ExpiryDate)
		.append(", Status=").append(this.Status).append(" ]");
		
		return sb.toString();
	}

}
