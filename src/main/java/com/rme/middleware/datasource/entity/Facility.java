package com.rme.middleware.datasource.entity;

public class Facility {
	
	private String Identifier;
	private String SequenceNumber;

	private String Description;
	private String CustomerName;
	private String RelatedPartyIdentifier;
	private String StartDate;
	private String ExpiryDate;
	private String Currency;
	private String LimitAmount;
	private String MultiCurrency;
	private String ExposureAmount;
	private String AvailableAmount;
	private String Status;
	
	public Facility(String identifier, String sequenceNumber, String description, String customerName, String relatedPartyIdentifier, String startDate,
			String expiryDate, String currency, String limitAmount, String multiCurrency, String exposureAmount,
			String availableAmount, String status) {

		this.Identifier = identifier;
		this.SequenceNumber = sequenceNumber;
		this.Description = description;
		this.CustomerName = customerName;
		this.RelatedPartyIdentifier = relatedPartyIdentifier;
		this.StartDate = startDate;
		this.ExpiryDate = expiryDate;
		this.Currency = currency;
		this.LimitAmount = limitAmount;
		this.MultiCurrency = multiCurrency;
		this.ExposureAmount = exposureAmount;
		this.AvailableAmount = availableAmount;
		this.Status = status;
	}

	public String getIdentifier() {
		return Identifier;
	}

	public void setIdentifier(String identifier) {
		Identifier = identifier;
	}

	public String getSequenceNumber() {
		return SequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		SequenceNumber = sequenceNumber;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getExpiryDate() {
		return ExpiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getLimitAmount() {
		return LimitAmount;
	}

	public void setLimitAmount(String limitAmount) {
		LimitAmount = limitAmount;
	}

	public String getMultiCurrency() {
		return MultiCurrency;
	}

	public void setMultiCurrency(String multiCurrency) {
		MultiCurrency = multiCurrency;
	}

	public String getExposureAmount() {
		return ExposureAmount;
	}

	public void setExposureAmount(String exposureAmount) {
		ExposureAmount = exposureAmount;
	}

	public String getAvailableAmount() {
		return AvailableAmount;
	}

	public void setAvailableAmount(String availableAmount) {
		AvailableAmount = availableAmount;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getRelatedPartyIdentifier() {
		return RelatedPartyIdentifier;
	}

	public void setRelatedPartyIdentifier(String relatedPartyIdentifier) {
		RelatedPartyIdentifier = relatedPartyIdentifier;
	}

}
